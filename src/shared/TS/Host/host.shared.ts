import { Error, NextFunction, Request, Response } from "interfaces/IExpress";

export function getFullHostUrl(language: string = "en"): string {
	if (process.env.WEBPACK) {
		return `${location.protocol}//${location.host}`;
	}

	let address: string = process.env.SERVER || "";
	const app = (global as any)["app"];
	const host = app.get("host");

	let indexSlashes = address.indexOf("//");
	address = host
		? address.substring(0, indexSlashes + 2) +
			host 
		: address.substring(0, indexSlashes + 2) +
			language +
			"." +
			address.substring(indexSlashes + 2, address.length);
	return address;
}

export function getGeneralDomain(language: string = "en"): string {
	let address: string = process.env.SERVER || "";

	let indexSlashes = address.indexOf("//");
	address = address.substring(indexSlashes + 2, address.length);
	return address;
}
