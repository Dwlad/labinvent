'use strict';

import * as http from "http";

/**
 * Module dependencies.
 */
import app from "../app";

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
process.env.BABEL_ENV = "node";

require('babel-core/register');
[
    '.css',
    '.less',
    '.sass',
    '.scsss',
    '.ttf',
    '.woff',
    '.woff2',
    ".jpg"
].forEach((ext) => require.extensions[ext] = () => { });
require('babel-polyfill');



/**
 * Get port from environment and store in Express.
 */
const port = normalizePort(process.env.PORT || 8080);
app.set('port', port);

/**
 * Create HTTP server.
 */

var server = http.createServer(app);

/**
 * Listen on provided port,on all network interfaces.
 */

server.listen(port, onListening);
server.on('error', onError);






/**
 * Normalize a port into a number,string,or false.
 */
/**
 * 
 * 
 * @param {*} val 
 * @returns {(number | string | boolean)} 
 */
function normalizePort(val: any): number | string | boolean {
    // let port = parseInt(val, 10);// + (parseInt(process.env.NODE_APP_INSTANCE) || 0);
    let port = parseInt(val, 10) + (parseInt(process.env.NODE_APP_INSTANCE || "0") || 0);


    if (isNaN(port)) {
        //name pipe
        return val;
    }

    if (port >= 0) {
        return port;
    }

    return false;
}

/**
 * Event listener for HTTP server "error" event.
 */
function onError(error: NodeJS.ErrnoException) {
    if (error.syscall != 'listen') {
        throw error;
    }

    var bind = typeof port === 'string'
        ? 'Pipe ' + port
        : 'Port ' + port;

    //handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + 'requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + 'is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */
function onListening() {
    var addr = server.address();
    var bind = typeof addr === 'string'
        ? 'pipe ' + addr
        : 'port ' + addr.port;
    console.log('Listening on ' + bind);
}