import * as moment from "moment";
export class Config {
	title = (() => {
		return this.getData({
			ru: "Gendree - откройся миру",
			en: "Gendree - open your mind"
		});
	})();

	keywords = (() => {
		return this.getData({
			ru:
				"социальная сеть, агендер, гей, геи, лесбиянка, лесбиянки, бесполое общество, бесполый, гендер, гендеры, равноправие, gendree, gendree.com",
			en:
				"social network, free gender, free race, free nationality, free religion, gendree, gendree.com"
		});
	})();

	description = (() => {
		return this.getData({
			ru:
				'Gendree - от английского "gender free". Данный сайт предназначен для полного отказа от предубеждений и средневековых оков, которые сдерживают нас в прошлых столетиях. Добро пожаловать в общество будущего!',
			en:
				"Gendree means gender free. Our social network is for all people, who doesn't care about gender, race, ideology, nationality etc."
		});
	})();

	private getData(dictionary: { ru: string; en: string }) {
		let locale = "en";
		if (process.env.WEBPACK) {
			locale = moment.locale();
		} else {
			const app = (global as any)["app"];
			locale = app.get("language");
		}

		switch (locale) {
			case "ru":
				return dictionary.ru;
			default:
				return dictionary.en;
		}
	}
}
