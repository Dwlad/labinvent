import { HttpError } from "shared/Error/http.error";
import { IWiFi } from "interfaces";
import { Types } from "mongoose";
import { isEqual } from "lodash";
import { WiFiModel } from "models/wiFi.model";
export function countWiFi(): Promise<any> {
	const promise: Promise<number | null> = new Promise(
		async (resolve, reject) => {
			try {
				const count = await WiFiModel.count({});

				resolve(count);
			} catch (err) {
				reject(err);
			}
		}
	);

	return promise;
}

export function initWiFiModelDb(): Promise<any> {
	const promise: Promise<null> = new Promise(async (resolve, reject) => {
		try {
			const count = await countWiFi();

			if (count === 0) {
				resolve();
			}

			const mock: IWiFi[] = require("mock/mock.json");

			mock.forEach(async wiFi => {
				try {
					const newWiFi = new WiFiModel({
						...wiFi,
						created: Date.now()
					});
					await newWiFi.save();
					resolve();
				} catch (e) {
					reject(e);
				}
				resolve;
			});
			resolve(count);
		} catch (err) {
			reject(err);
		}
	});

	return promise;
}

export function getAllWiFi(): Promise<any> {
	const promise: Promise<IWiFi[] | null> = new Promise(
		async (resolve, reject) => {
			try {
				const networks = await WiFiModel.find({});

				resolve(networks);
			} catch (err) {
				reject(err);
			}
		}
	);

	return promise;
}
