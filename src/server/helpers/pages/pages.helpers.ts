import { Helmet, HelmetData } from "react-helmet";
import { NextFunction, Request, Response } from "interfaces/IExpress";

import { Config } from "configures/html.configure";

const ROOT_DIR: string = process.cwd();
const assets = require(ROOT_DIR + "/app/server/public/assets.json");
const assetsVendor = require(ROOT_DIR +
	"/app/server/public/assets_vendor.json");
export function renderProperty(
	req: Request,
	res: Response,
	title: {str?:string,intl?:string},
	reactApp?: string,
	styles?: string,
	scripts?: string,
	app?: string,
	state?: JSON,
	helmet?: HelmetData
): any {
	let htmlTitle = "";
	const config = new Config();
	if (helmet) {
		if (helmet.title) {
			const helmetTitle = helmet.title.toString();
			const start = helmetTitle.indexOf(">") + 1;
			const end = helmetTitle.lastIndexOf("<");

			htmlTitle = helmetTitle.substr(start, end - start);
		}
	} else {
		const text = title.intl ? res.intl("titles."+title.intl) : title.str
		htmlTitle = text ? `${text} | ${config.title}` : `${config.title}`;
	}

	const result: any = {
		title: htmlTitle,
		keywords: config.keywords,
		description: config.description,
		commonJs: assets.common.js,
		//commonScss: assets.common.css,
		vendor: assetsVendor.vendor.js,
		//globalScss: assets.globalCss.css,
		//globalScssJs: assets.globalCss.js,
		//globalJs: assets.globalJs.js,
		//socketJs: assets.socketJs.js,
		showCookieMessage:
			req.cookies.cookiePolicy && req.cookies.cookiePolicy === "true",
	
	};

	if (app) {
		result.app = app;
	}

	if (state) {
		result.state = JSON.stringify(state);
	}

	if (reactApp) {
		result.reactJs = assets[reactApp].js;
        result.reactCss = assets[reactApp].css;
    	}

	if (styles) {
		result.pageScss = assets[styles].css;
		result.pageScssJs = assets[styles].js;
	}

	if (scripts) {
		result.pageJs = assets[scripts].js;
	}

	return result;
}