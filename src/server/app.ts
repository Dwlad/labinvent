"use strict";

import "TS/Javascript/String/string.helpers";

import * as ExpressVender from "./vendors/index";
import * as bodyParser from "body-parser";
import * as compression from "compression";
import * as connect from "connect-mongo";
import * as cookieParser from "cookie-parser";
import * as express from "express";
import * as logger from "morgan";
import * as mongoose from "mongoose";
import * as morgan from "morgan";
import * as path from "path";
import * as session from "express-session";
import * as moment from "moment";
import { Error, NextFunction, Request, Response } from "interfaces/IExpress";

import { Config } from "configures/html.configure";
import api from "routes/api";
import config from "configures/index";
import { getGeneralDomain } from "TS/Host/host.shared";
import pages from "routes/pages/pages.routes";
import { sessionStore } from "shared/Session/session.shared";
import { winstonStream } from "log/winston.logger";
import { initWiFiModelDb } from "helpers/wiFi/wiFi.helpers";
const Window = require("window");

const ROOT_DIR: string = process.cwd();
const assets = require(ROOT_DIR + "/app/server/public/assets.json");
const assetsVendor = require(ROOT_DIR +
	"/app/server/public/assets_vendor.json");

const app: express.Express = express();
(global as any)["app"] = app;
//import routes from "./routes/index"; use gzip
app.enable("trust proxy");
app.use(
	logger("common", {
		stream: winstonStream as any
	})
);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(sessionStore);

// app.use((req: Request, res: Response, next: NextFunction) => {
// 	res.sendError = ExpressVender.sendError;
// 	next();
// });

require("./db");

initWiFiModelDb().catch(err => console.log("WiFi init error", err));
app.use(compression());

// app.use(function(req, res, next) {
// 	res.header("Access-Control-Allow-Origin", "*");
// 	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
// 	next();
//   });

// app.use((req: Request, res: Response, next: NextFunction) => {
// 	console.log(req.app.get("test"), req.getLocale())

// 	next();
// });

//view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

// uncomment after placing your favicon in /public
// app.use(favicon(path.join(__dirname,'public','favicon.ico')));
// app.use(logger('combined'));

app.use(express.static(path.join(__dirname, "public")));

if (process.env.NODE_ENV !== "production" && !process.env.NODEMON) {
	const webpackDevHelper = require("../../system/webpack/index.dev");
	console.log("DEVOLOPMENT ENVIRONMENT: Turning on WebPack Middleware...");
	webpackDevHelper.useWebpackMiddleware(app);
}

// Production needs physical files! (built via separate process)
// app.use(express.static(path.join(__dirname, 'public'))); app.use('/js',
// express.static(__dirname + '/public/js'));

app.use("/api", api);

app.use("/", pages);

//catch 404 and forward to error handler
app.use((req: Request, res: Response, next: NextFunction) => {
	var err: Error = new Error("Not Found");
	err["status"] = 404;
	next(err);
});

//error handlers development error handler will print stacktrace
if (process.env.NODE_ENV === "development") {
	app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
		res.status(err["status"] || 500);
		res.render("error", {
			message: err.message,
			error: err,
			title: err.message + ` - ${config.get("slogan")}`,
			// app, // Server render app reactJs: assets.indexApp.js, reactCss:
			// assets.indexApp.css,
			commonJs: assets.common.js,

			vendor: assetsVendor.vendor.js,
			//globalScss: assets.globalCss.css,
			//globalScssJs: assets.globalCss.js,
			//

			globalJs: assets.globalJs.js,
			showCookieMessage:
				req.cookies &&
				req.cookies.cookiePolicy &&
				req.cookies.cookiePolicy === "true"
		});
	});
}

//production error handler no stacktrace leaked to user
app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
	res.status(err["status"] || 500);
	res.render("error", {
		message: err.message,
		error: {},
		title: err.message + ` | ${new Config().title}`,
		// app, // Server render app reactJs: assets.indexApp.js, reactCss:
		// assets.indexApp.css,
		commonJs: assets.common.js,

		vendor: assetsVendor.vendor.js,
		//globalScss: assets.globalCss.css,
		//globalScssJs: assets.globalCss.js,
		//globalJs: assets.globalJs.js,
		showCookieMessage:
			req.cookies &&
			req.cookies.cookiePolicy &&
			req.cookies.cookiePolicy === "true"
	});
});

export default app;
