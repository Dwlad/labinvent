import * as mongoose from "mongoose";

import config from "configures/index";

const ROOT_PATH = process.cwd();

(mongoose as any).Promise = Promise;

console.log("mongoose");
if (process.env.NODE_ENV === "production") {
	console.log("production server");
	(mongoose as any).connect(config.get("mongoose:uri"), {
		useMongoClient: true
		/* other options */
	});
} else {
	console.log("development server");
	(mongoose as any).connect(config.get("mongoose:uriStage"), {
		useMongoClient: true
		/* other options */
	});
}

var db = mongoose.connection;

db.on("error", function(err: NodeJS.ErrnoException) {
	console.log("Connection error:", err.message);
});

db.once("open", function callback() {
	console.log("Connected to DB!");
});

process.on("exit", (code: Object) => {
	console.log(`About to exit with code: ${code}`);
});

process.on("disconnect", () => {
	console.log(`disconnect`);
});


process.on("uncaughtException", (err: any) => {
	console.log(err);
});

export default mongoose;
