import * as winstone from "winston";
const transports:any[] = [
    new winstone
        .transports
        .Console({ level: "silly", handleExceptions: true, json: false, colorize: true })
];

if (process.env.NODE_ENV === "production") {
    transports.push(new winstone.transports.File({ level: "error", name: 'error-file', filename: './log-error.log', handleExceptions: true }));
    transports.push(new winstone.transports.File({ level: "info", name: 'info-file', filename: './log-info.log', handleExceptions: true }));
}

const logger = new winstone.Logger({
    transports,
    exitOnError: false
})

export const winstonStream = {
    write: function (message: any, encoding: any) {
        logger.info(message);
    }
}