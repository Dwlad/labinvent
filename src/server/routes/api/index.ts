import * as express from "express";

import { Request, Response } from "interfaces/IExpress";



const router = express.Router();
require("./wiFi.api.routes")(router);


export default router;