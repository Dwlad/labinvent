import * as Helmet from "react-helmet";
import * as express from "express";

import { IndexPage } from "../../controllers";
import { NextFunction, Request, Response } from "interfaces/IExpress";

import Render from "../../shared/Render";
import { Router } from 'react-router';
import {  MessagesController } from "controllers/pages";

const ROOT_DIR: string = process.cwd();
const assets = require(ROOT_DIR + '/app/server/public/assets.json');
const assetsVendor = require(ROOT_DIR + '/app/server/public/assets_vendor.json');

const router = express.Router();

/* GET home page. */
router.get('/', IndexPage);
/* GET home page. */
// router.get(["/messages","/messages/:username"], MessagesController);


/*
router.get('/:id', (req: Request, res: Response, next) => {
    let actions: Function[] = [];


    switch (req.params.id) {
        case "
            actions.push(timeRequest, test.bind(this, "eee"));
            break;
        default:
            actions = [];
    }

    if (actions.length > 0) {
        Render.pageWithDispatch(req, res, configureStore, routes, actions, (app: string, state: JSON) => {
            let head = Helmet.rewind();
            console.log(head.title.toString().replace('<title data-react-helmet="true">',"").replace("</title>",""));
            res.render('index', {
                title: head.title.toString().replace('<title data-react-helmet="true">',"").replace("</title>","") || 'Gendree - first gender free social network',
                app,
                //app: renderToString(React.createElement(RouterContext,renderProps)),
                reactJs: assets.indexApp.js,
                reactCss: assets.indexApp.css,
                commonJs: assets.common.js,
                state: JSON.stringify(state),
                vendor: assetsVendor.vendor.js
            });
        });
    } else {
        Render.page(req, res, configureStore, routes, (app: string, state: JSON) => {
            res.render('index', {
                title:'Gendree - first gender free social network',
                app,
                //app: renderToString(React.createElement(RouterContext,renderProps)),
                reactJs: assets.indexApp.js,
                reactCss: assets.indexApp.css,
                commonJs: assets.common.js,
                vendor: assetsVendor.vendor.js
            });
        });
    }
    //res.render('index', {title: 'BVVStudio | Студия разработки сайтов'});
});
*/

export default router;
