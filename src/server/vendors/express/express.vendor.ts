import {Config} from "configures/html.configure";
import {HttpError} from "shared/Error/http.error";
const ROOT_DIR : string = process.cwd();
const assets = require(ROOT_DIR + '/app/server/public/assets.json');
const assetsVendor = require(ROOT_DIR + '/app/server/public/assets_vendor.json');
export function sendError(err : HttpError, isApi = true, req = {
    language: "en",
    cookies: {
        cookiePolicy: "false"
    },
    getLocale:()=>"en"
}) {
    this.status(err['statusCode'] || 500);
    if (isApi === true) {
        this.send(err);
    } else {

        this.render('error', {
            message: err.message,
            error: err,
            title: err.message + ` | ${new Config().title}`,
            // app, // Server render app reactJs: assets.indexApp.js, reactCss:
            // assets.indexApp.css,
            commonJs: assets.common.js,

            vendor: assetsVendor.vendor.js,
            globalScss: assets.globalCss.css,
            globalScssJs: assets.globalCss.js,
            globalJs: assets.globalJs.js,
            showCookieMessage: req.cookies.cookiePolicy && req.cookies.cookiePolicy === "true"
        });
    }
}