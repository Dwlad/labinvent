import * as mongoose from "mongoose";

import {IWiFi} from "interfaces";

const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;

// AccessToken
var WiFI = new Schema({

    name: {
        type: String,
        required:true
    },
    favorite:{
        reqiured:true,
        type:Boolean
    },
    strength:{
        reqiured:true,
        type:Number
    },
    security:[{
        reqiured:true,
        type:String
    }],
    
    created: {
        type: Number,
        required: true,
        default: Date.now()
    }    
});

export const WiFiModel = mongoose.model < IWiFi > ('WiFi', WiFI);
