import { NextFunction, Request, Response } from "interfaces/IExpress";

import { Helmet } from "react-helmet";
import Render from "shared/Render";
import { Roles } from "shared/Enums/enum.shared";
import { renderProperty } from "helpers/pages/pages.helpers";
import { Types } from "mongoose";
const ROOT_DIR: string = process.cwd();
const assets = require(ROOT_DIR + "/app/server/public/assets.json");
const assetsVendor = require(ROOT_DIR +
	"/app/server/public/assets_vendor.json");
import MessageAppConfigureStore from "indexApp/store/configureStore.search";
import { indexAppRoutes } from "indexApp/routes/routes.search";
export function IndexPage(req: Request, res: Response, next: NextFunction) {
	let actions: Function[] = [];

	Render.pageWithDispatch(
		req,
		res,
		MessageAppConfigureStore,
		indexAppRoutes,
		actions,
		"indexApp",
		(app: string, state: JSON) => {
			const render = renderProperty(
				req,
				res,
				req.params.username,
				"indexApp",
				undefined,
				undefined,
				app,
				state,
				Helmet.renderStatic()
			);
			res.render("index", render);
		}
	);
}
