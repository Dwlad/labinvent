import * as React from "react";
import * as async from "async";
import * as moment from "moment";

import { Error, Request, Response } from "interfaces/IExpress";

import { Helmet } from "react-helmet";
import { Provider } from "react-redux";
import { StaticRouter } from "react-router-dom";
import { renderToString } from "react-dom/server";

import Element = JSX.Element;

class Render {
	static pageWithDispatch = (
		req: Request,
		res: Response,
		configureStore: Function,
		Routes: () => Element,
		actions: Function[],
		reactApp: string,
		callback: Function
	) => {
		


		const store = configureStore();
		
		async.each(
			actions,
			(action: Function, callback: Function) => {
				store
					.dispatch(action())
					.then((a: any) => {
						return callback();
					})
					.catch((err: any) => {
						console.log(action);
						console.log("Server rendering dispatch error: " + err);
						callback(err);
						return;
					});
			},
			(err: Error) => {
				if (err) {
					console.log("Server redux error", err);
					throw err;
				}
				const state = store.getState();
				

				const app = renderToString(
					React.createElement(
						Provider,
						{ store },
							React.createElement(
								StaticRouter,
								{ context: {}, location: req.url },
								React.createElement(Routes, {})
							)
						
					)
				);
				callback(app, state);
			}
		);
	};

	static page = (
		req: Request,
		res: Response,
		configureStore: Function,
		routes: () => Element,
		reactApp: string,
		callback: Function
	) => {
		const store = configureStore();
	
		const state = store.getState();
	
		
		const app = renderToString(
			React.createElement(
				Provider,
				{ store },
			
					React.createElement(
						StaticRouter,
						{ context: {}, location: req.url },
						React.createElement(routes, {})
					)
				
			)
		);
		callback(app, state);
	};
}

export default Render;
