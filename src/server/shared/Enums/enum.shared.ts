// export class Roles {     private value : string;     constructor(value :
// string) {         this.value = value;     }     toString() {         return
// this.value;     }     getValue() {         return this.value;     } static
// user = new Roles("user");     static guest = new Roles("guest"); static admin
// = new Roles("admin"); }

export enum Roles {
    admin = 3287562389756298374,
    guest = 23085298352937462384,
    user = 2857924356237462385
}

export class Rights {

    private value : string;

    constructor(value : string) {
        this.value = value;
    }

    toString() {
        return this.value;
    }

    getValue() {
        return this.value;
    }

    static members = new Rights("members");
    static admins = new Rights("admins");
    static founders = new Rights("founders");
    static moderators = new Rights("moderators");
    static guests = new Rights("guests");
}