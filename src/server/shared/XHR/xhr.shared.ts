import {Error, NextFunction, Request, Response} from "interfaces/IExpress";

export default class XHR {
    static checkXMLHttpRequest(req : Request) : boolean {
        return req.xhr || req
            .headers['accept']!
            .indexOf('json') > -1
    }
}