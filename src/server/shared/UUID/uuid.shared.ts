export class Uuid {
    private getRandomInt = (min: number, max: number) => {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    };

    generate() {

        let buf: Array<string> = []
            , chars: string = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
            , charlen: number = chars.length;

        for (var i = 0; i < 36; ++i) {
            if (i === 8 || i === 13 || i === 18 || i === 23) buf.push("-");
            else buf.push(chars[this.getRandomInt(0, charlen - 1)]);
        }
        return buf.join('');
    }
}