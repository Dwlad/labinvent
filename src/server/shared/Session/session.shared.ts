import * as connect from "connect-mongo";
import * as mongoose from "mongoose";
import * as session from "express-session";

import config from "configures/index";
import { getGeneralDomain } from "TS/Host/host.shared";

const MongoStore = connect(session);



export const sessionStore = session({
	secret: config.get("secret"),
	resave: false,
	saveUninitialized: false,
	cookie: { domain: "." + getGeneralDomain() ,secure:true},
	store: new MongoStore({ mongooseConnection: mongoose.connection })
});