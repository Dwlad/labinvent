import * as mongoose from "mongoose";

import {Types} from "mongoose";


export interface IWiFi extends mongoose.Document {
    _id: Types.ObjectId;
    favorite:boolean;
    strength:number;
    security:string[];
}
