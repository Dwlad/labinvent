import {HttpError} from "shared/Error/http.error";

export interface IReduxGeneric < T > {
    data: T;
    loading: boolean;
    error: HttpError | undefined;
}

export interface IReduxArrayGeneric < T > {
    data: T[];
    loading: boolean;
    error: HttpError | undefined | null;
}
