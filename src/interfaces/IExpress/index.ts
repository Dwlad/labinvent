import * as express from "express";

import { HttpError } from "shared/Error/http.error";

interface ICookie extends Express.SessionCookie {
	maxAge: number;
	username: string;
}
interface ISession extends Express.Session {
	cookie: ICookie;
	isNewUser: boolean | undefined;
}

interface IHeader {
	accept: string;
}

export interface Request extends express.Request {
	session: ISession;
}

export interface Response extends express.Response {
	sendError(err: HttpError, isApi?: boolean, request?: Request): void;
	intl(path:string):string;
}

export interface NextFunction extends express.NextFunction {}

export interface Error {
	status?: number;
	message: string;
}
