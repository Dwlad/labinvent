import {HttpError} from "shared/Error/http.error";

export interface IAction {
    type : string;
    payload?: any;
    error?: HttpError;
}
