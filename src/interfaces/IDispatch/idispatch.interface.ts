import {IAction} from "interfaces";

export interface IDispatch {
    (action : IAction) : void;
};