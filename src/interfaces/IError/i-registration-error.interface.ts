export interface IRegistrationError extends Error {
    code : number;
    message : string;
    action?: string;
    username?: string;
    name : string;
    email?: string;
}