export async function catchResponseError(response : Response) {
    if (response.status >= 400) {
        try {
            const json = await response.json();
            return Promise.reject(json);

        } catch (err) {
            return Promise.reject(err);

        }
    }

    return response;
}