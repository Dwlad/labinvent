import * as React from "react";
export function passPropsToChild(Component: any, props: any,additional:any={}) {
    let result = (Object as any).assign({},additional,props);
    return <Component {...result} />
}
