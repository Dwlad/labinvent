import * as React from "react";
import * as classnames from "classnames";

export interface InputTextProps {
	lable: string;

	disabled: boolean;
	required: boolean;
}

export function InputTextComponent(props: InputTextProps) {
	return (
		<div className="form-group row  app__dhcp__ip">
			<label
				className={classnames(
					{ "text-muted": props.disabled },
					"col-5 col-form-label text-right "
				)}
			>
				{props.lable}
				{props.required && <span>*</span>}
			</label>
			<div className="col-7">
				<input
					type="text"
					className="form-control"
					id="staticEmail"
					required={props.required}
					disabled={props.disabled}
				/>
			</div>
		</div>
	);
}
