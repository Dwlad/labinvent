import * as React from "react";

if (process.env.WEBPACK) {
	//  require ("./html-programming.jpg");
	require("./checkbox.component.scss");
}
export interface CheckboxProps {
    title: string;

    checked:boolean;
}

export function CheckboxComponent(props: CheckboxProps) {
	return (
		<div className="app__checkbox">
			<label>
				<input type="checkbox" checked={props.checked} />
				{props.title}
			</label>
		</div>
	);
}
