import * as React from "react";

if (process.env.WEBPACK) {
	//  require ("./html-programming.jpg");
	require("./radioButton.component.scss");
}
export interface RadioButtonProps {
    title: string;
    value:string;

    checked:boolean;
}

export function RadioButtonComponent(props: RadioButtonProps) {
	return (
		<div className="app__radio">
			<label>
				<input type="radio" value={props.value} checked={props.checked} />
				{props.title}
			</label>
		</div>
	);
}
