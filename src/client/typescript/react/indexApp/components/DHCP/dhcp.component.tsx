import * as React from "react";
import { RadioButtonComponent } from "indexApp/components/RadioButton/radioButton.component";
import { InputTextComponent } from "indexApp/components/InputText/inputText.component";

if (process.env.WEBPACK) {
	//  require ("./html-programming.jpg");
	require("./dhcp.component.scss");
}
export interface DhcpProps {}

export class DhcpComponent extends React.Component<DhcpProps, any> {
	render() {
		return (
			<div className="app__dhcp">
				<RadioButtonComponent
					title="Obtain an IP address automatically (DHCP/BootP)"
					value="dhcp"
					checked={true}
				/>

				<RadioButtonComponent
					title="Use the following IP address"
					value="handledIP"
					checked={false}
				/>
				<InputTextComponent
					lable="IP address:"
					disabled={true}
					required={true}
				/>
				<InputTextComponent
					lable="Subnet Mask:"
					disabled={true}
					required={true}
				/>
				<InputTextComponent
					lable="DefaultGetway:"
					disabled={true}
					required={false}
				/>
			</div>
		);
	}
}
