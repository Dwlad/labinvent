import * as React from "react";
import { RadioButtonComponent } from "indexApp/components/RadioButton/radioButton.component";
import { InputTextComponent } from "indexApp/components/InputText/inputText.component";
import { CheckboxComponent } from "indexApp/components/Checkbox/checkbox.component";
import Select from "react-select";

if (process.env.WEBPACK) {
	require("react-select/dist/react-select.css");
}
export interface WiFiProps {}
export interface WiFiState {
	selectedOption: any;
}

export class WiFiComponent extends React.Component<WiFiProps, WiFiState> {

	state:WiFiState = {
		selectedOption: ""
	};
	handleChange = (selectedOption: any) => {
		this.setState({ selectedOption });
		console.log(`Selected: ${selectedOption.label}`);
	};
	render() {
		console.log(this.state);
		const { selectedOption } = this.state;
		const value = selectedOption && selectedOption.value;
		return (
			<div className="app__dns">
				<CheckboxComponent title="Enable wifi:" checked={false} />
				<InputTextComponent
					lable="Wireless Network Name:"
					disabled={true}
					required={true}
				/>

				<Select
					name="form-field-name"
					value={value}
					onChange={(e) => this.handleChange(e)}
					options={[
						{ value: "one", label: "One" },
						{ value: "two", label: "Two" }
					]}
				/>
				<CheckboxComponent
					title="Enable Wireless Security"
					checked={false}
				/>

				<InputTextComponent
					lable="Security Key:"
					disabled={true}
					required={false}
				/>
			</div>
		);
	}
}
