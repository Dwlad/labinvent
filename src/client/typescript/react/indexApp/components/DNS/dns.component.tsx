import * as React from "react";
import { RadioButtonComponent } from "indexApp/components/RadioButton/radioButton.component";
import { InputTextComponent } from "indexApp/components/InputText/inputText.component";

export interface DnsProps {}

export class DnsComponent extends React.Component<DnsProps, any> {
	render() {
		return (
			<div className="app__dns">
				<RadioButtonComponent
					title="Obtain DNS server address automatically"
					value="autoDNS"
					checked={true}
				/>
				<RadioButtonComponent
					title="Use the following DS server address"
					value="handledDNS"
					checked={false}
				/>

				<InputTextComponent
					lable="Preferred DNS server:"
					disabled={true}
					required={true}
				/>
				<InputTextComponent
					lable="Alternative DNS server:"
					disabled={true}
					required={false}
				/>
			</div>
		);
	}
}
