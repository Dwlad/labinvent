import * as React from "react";

import { Layout } from "indexApp/containers"
import { Route, Switch } from "react-router";

import { Provider } from "react-redux";

/*export default (
    <Router history={browserHistory}>
        <Route component={Page} path="/users">
            <IndexRoute component={Users} />
            <Route component={Users} path="*" />
        </Route>
    </Router>
);*/

/*export const UsersAppRoutes: RouteConfig[] = [
    {
        component: Page,
        routes: [
            {
                component: Users as any,
                path: '/users'
            }
        ]
    }

]*/

export const indexAppRoutes = () => (
  <Switch>
    {/* some other routes */}
    <Route render={()=><Layout />} />
  </Switch>
)