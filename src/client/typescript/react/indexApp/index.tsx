import * as React from "react";
import * as ReactDom from "react-dom";
import axios from "axios";

import { AppContainer } from "react-hot-loader";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import { indexAppRoutes } from "indexApp/routes/routes.search";
import configureStore from "indexApp/store/configureStore.search";

const Window: any = window;
const initialState: any = Window ? Window["REDUX_INITIAL_STATE"] : {};
const store = configureStore(initialState);
if (Window && Window["REDUX_INITIAL_STATE"]) {
	Window["REDUX_INITIAL_STATE"] = null;
}



const render = (Component: any) => {
	try {
		ReactDom.hydrate(
			<AppContainer>
				<Provider key={module.hot ? Date.now() : store} store={store}>
						<BrowserRouter>
							<Component />
						</BrowserRouter>
				</Provider>
			</AppContainer>,
			document.getElementById("root")
		);
	} catch (err) {
		console.error(err);
	}
};

render(indexAppRoutes);
if (module.hot) {
	module.hot.accept("indexApp/routes/routes.search", () =>
		render(require("indexApp/routes/routes.search").indexAppRoutes)
	);
}
