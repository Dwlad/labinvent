import * as React from "react";
import * as Redux from "redux";
import * as QueryString from "query-string";
import { Helmet } from "react-helmet";

import { isEqual } from "lodash";

import { IReduxArrayGeneric, IReduxGeneric } from "interfaces";
import { ISearchActions, handleSearch } from "indexApp/actions";
import { Route, Switch, withRouter, NavLink } from "react-router-dom";

import { Component } from "react";
import { passPropsToChild } from "Shared/PassPropsToChild/pass-props-to.child.shared";
import { ReactElement } from "react";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router";
import { PanelContainer } from "indexApp/containers";
import { RadioButtonComponent } from "indexApp/components/RadioButton/radioButton.component";
import { DhcpComponent } from "indexApp/components/DHCP/dhcp.component";
import { DnsComponent } from "indexApp/components/DNS/dns.component";
import { WiFiComponent } from "indexApp/components/WiFi/wiFi.component";

if (process.env.WEBPACK) {
	//  require ("./html-programming.jpg");
	require("./layout.container.scss");
}
interface IParams {}
export interface IRoute {
	path: string | undefined;
}

export interface ISearch {
	phrase: string;
}
interface IActions {
	searchActions: ISearchActions;
}
interface IProperties {
	route?: IRoute;
}
interface PageProperty extends IProperties, IActions, RouteComponentProps<any> {
	params: IParams;
}

interface ReduxState {}
interface PageState {
	search?: ISearch;
}
export class LayoutContainer extends React.PureComponent<
	PageProperty,
	PageState
> {
	constructor(props: PageProperty) {
		super(props);
	}

	render() {
		const { params, route, location } = this.props;
		let result = null;
		result = (
			<form>
				<div className="row">
					<PanelContainer title={"Ethernet Settings"}>
						<DhcpComponent />
						<DnsComponent />
					</PanelContainer>
					<PanelContainer title={"Wireless Settings"}>
						<WiFiComponent />
						<DhcpComponent />
						<DnsComponent />
					</PanelContainer>
				</div>
			</form>
		);
		// }
		return (
			<main className="container app__layout">
				{result}
				<Helmet>
					<title>Лаборатория изобретений</title>
				</Helmet>
			</main>
		);
	}
}

function mapStateToProps(state: ReduxState): IProperties {
	return {};
}

const mapDispatchToProps = (
	dispatch: Redux.Dispatch<PageProperty>
): IActions => {
	return {
		searchActions: Redux.bindActionCreators(
			{
				handleSearch
			},
			dispatch
		)
	};
};

const Connect = connect(mapStateToProps, mapDispatchToProps)(LayoutContainer);

export const Layout = (withRouter as any)(Connect);
