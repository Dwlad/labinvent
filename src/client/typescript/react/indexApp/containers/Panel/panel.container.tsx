
import * as React from "react";

import {
	IReduxArrayGeneric,
	IReduxGeneric,
} from "interfaces";

import { ReactElement } from "react";

import { RouteComponentProps } from "react-router";

if (process.env.WEBPACK) {
	//require('./images/1920x1080.jpg');
	require("./panel.container.scss");
}
export interface PanelProps {
	title:string;
}

export class PanelContainer extends React.PureComponent<PanelProps, any> {


	render() {
		
		const {title,children} = this.props;

		return (
			<section className="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
				<h6>{title}</h6>
				{children}
			</section>
		);
	}
}
