import { applyMiddleware, compose, createStore } from "redux";

import { Reducers } from "indexApp/reducers";
import thunk from "redux-thunk";


export default function(initialState: any = {}) {
	let store: any = undefined;
	const callback = (action: Error) =>
		console.error(
			`${action} didn't lead to creation of a new state object`
		);
	if (typeof window !== "undefined") {
		let test: any = window;

		store = createStore(
			Reducers,
			initialState,
			compose(
				applyMiddleware(
					thunk
				),
				process.env.NODE_ENV !== "production" && test.devToolsExtension
					? test.devToolsExtension()
					: (f: any) => f
			)
		);
	} else {
		store = createStore(
			Reducers,
			initialState,
			applyMiddleware(
				thunk 
			)
		);
	}

	const NodeModule: any = module;

	if (NodeModule.hot) {
		NodeModule.hot.accept("../reducers", () => {
			store.replaceReducer(require("indexApp/reducers"));
		});
	}
	return store;
}
