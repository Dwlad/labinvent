import * as fetch from "isomorphic-fetch";

import {
	HANDLE_SEARCH_ERROR,
	HANDLE_SEARCH_REQUEST,
	HANDLE_SEARCH_SUCCESS
} from "indexApp/constants/search.constants";
import { IDispatch } from "interfaces";

import { Types } from "mongoose";
import { catchResponseError } from "Shared/actions/catchResponseError/catchResponseError.shared";
import axios from "axios";

import { getFullHostUrl } from "TS/Host/host.shared";
let address: string | undefined = undefined;
export function handleSearch(searchPhrase: string) {
	address = getFullHostUrl() || process.env.SERVER;
	return (dispatch: IDispatch) => {
		dispatch({ type: HANDLE_SEARCH_REQUEST });
		let url = `${address}/api/search?username=${searchPhrase}`;
		// if (token) {     url = `${url}?token=${token}` }
		const text = axios
			.get(`${address}/api/search`, {
				params: {
					username: searchPhrase
				}
			})
			.then(response => response.data)
			.then(data =>
				dispatch({ type: HANDLE_SEARCH_SUCCESS, payload: data })
			)
			.catch(err => {
				console.log(err);
				dispatch({
					type: HANDLE_SEARCH_ERROR,
					error: err.response.data
				});
			});
		//     const text = fetch(url, { "credentials": "same-origin" , headers: {
		//   'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
		// },})
		//         .then(response => catchResponseError(response))
		//         .then((response) => response.json())
		//         .then((json) => dispatch({ type: HANDLE_SEARCH_SUCCESS, payload: json }))
		//         .catch((err) => {
		//             console.log("err",err);
		//             dispatch({ type: HANDLE_SEARCH_ERROR, error: err });
		//             //return Promise.reject(err)
		//         });
		return text;
	};
}

export interface ISearchActions {
	handleSearch(searchPhrase: string): void;
}
