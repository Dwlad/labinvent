import { HANDLE_SEARCH_ERROR, HANDLE_SEARCH_REQUEST, HANDLE_SEARCH_SUCCESS } from "indexApp/constants/search.constants"

import { IAction } from "interfaces";

const initialState = {
    data: [],
    error: null,
    loading: false
};

export function wirelessReducer(state = initialState, action: IAction) {
    switch (action.type) {
        case HANDLE_SEARCH_REQUEST:
            return Object.assign({}, state, {
                loading: true,
                data: state.data || [],
                error: undefined
            });
        case HANDLE_SEARCH_SUCCESS:
            return Object.assign({}, state, {
                loading: false,
                error: undefined,
                data: action.payload
            });
        case HANDLE_SEARCH_ERROR:
            return Object.assign({}, state, {
                loading: false,
                error: action.error,
                data: []
            });
        default:
            return state;
    }
}