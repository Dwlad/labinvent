var beep =require( 'beepbeep');
//var browser  =require( 'browser-sync');
var colors =require( 'colors');
var fs =require( 'fs');
var gulp =require( 'gulp');
var inky =require( 'inky');
var lazypipe =require( 'lazypipe');
var merge =require( 'merge-stream');
var panini =require( 'panini');
var path =require( 'path');
var plugins =require( 'gulp-load-plugins');
var rimraf =require( 'rimraf');
var siphon =require( 'siphon-media-query');
var yargs =require( 'yargs');

const $ = plugins();// Look for the --production flag
const PRODUCTION = !!(yargs.argv.production);
const EMAIL = yargs.argv.to;


// Look for the --production flag

// Declar var so that both AWS and Litmus task can use it.
var CONFIG;

// Build the "dist" folder by running all of the below tasks
gulp.task('build',
  gulp.series(clean, pages, sass, images, inline));

// Build emails, run the server, and watch for file changes
gulp.task('default',
  gulp.series('build', watch));

// Build emails, then send to litmus
gulp.task('litmus',
  gulp.series('build', creds, aws, litmus));

// Build emails, then send to EMAIL
gulp.task('mail',
  gulp.series('build', creds, aws, mail));

// Build emails, then zip
gulp.task('zip',
  gulp.series('build', zip));

// Delete the "dist" folder
// This happens every time a build starts
function clean(done) {
  rimraf('app/email', done);
}

// Compile layouts, pages, and partials into flat HTML files
// Then parse using Inky templates
function pages() {
  return gulp.src(['src/email/pages/**/*.html', '!src/pages/archive/**/*.html'])
    .pipe(panini({
      root: 'src/email/pages',
      layouts: 'src/email/layouts',
      partials: 'src/email/partials',
      helpers: 'src/email/helpers'
    }))
    .pipe(inky())
    .pipe(gulp.dest('app/email'));
}

// Reset Panini's cache of layouts and partials
function resetPages(done) {
  panini.refresh();
  done();
}

// Compile Sass into CSS
function sass() {
  return gulp.src('src/email/assets/scss/app.scss')
    .pipe($.if(!PRODUCTION, $.sourcemaps.init()))
    .pipe($.sass({
      includePaths: ['node_modules/foundation-emails/scss']
    }).on('error', $.sass.logError))
    .pipe($.if(PRODUCTION, $.uncss(
      {
        html: ['app/email/**/*.html']
      })))
    .pipe($.if(!PRODUCTION, $.sourcemaps.write()))
    .pipe(gulp.dest('app/email/css'));
}

// Copy and compress images
function images() {
  return gulp.src(['src/email/assets/img/**/*', '!src/assets/img/archive/**/*'])
    .pipe($.imagemin())
    .pipe(gulp.dest('./app/email/assets/img'));
}

// Inline CSS and minify HTML
function inline() {
  return gulp.src('app/email/**/*.html')
    .pipe($.if(PRODUCTION, inliner('app/email/css/app.css')))
    .pipe(gulp.dest('app/email'));
}

// Start a server with LiveReload to preview the site in
function server(done) {
  browser.init({
    server: 'app/email'
  });
  done();
}

// Watch for file changes
function watch() {
  gulp.watch('src/email/pages/**/*.html').on('all', gulp.series(pages, inline));
  gulp.watch(['src/email/layouts/**/*', 'src/email/partials/**/*']).on('all', gulp.series(resetPages, pages, inline));
  gulp.watch(['src/email/scss/**/*.scss', 'src/email/assets/scss/**/*.scss']).on('all', gulp.series(resetPages, sass, pages, inline));
  gulp.watch('src/email/assets/img/**/*').on('all', gulp.series(images));
}

// Inlines CSS into HTML, adds media query CSS into the <style> tag of the email, and compresses the HTML
function inliner(css) {
  var css = fs.readFileSync(css).toString();
  var mqCss = siphon(css);

  var pipe = lazypipe()
    .pipe($.inlineCss, {
      applyStyleTags: false,
      removeStyleTags: true,
      preserveMediaQueries: true,
      removeLinkTags: false
    })
    .pipe($.replace, '<!-- <style> -->', `<style>${mqCss}</style>`)
    .pipe($.replace, '<link rel="stylesheet" type="text/css" href="app/email/css/app.css">', '')
    .pipe($.htmlmin, {
      collapseWhitespace: true,
      minifyCSS: true
    });

  return pipe();
}

// Ensure creds for Litmus are at least there.
function creds(done) {
  var configPath = './config.json';
  try { CONFIG = JSON.parse(fs.readFileSync(configPath)); }
  catch (e) {
    beep();
    console.log('[AWS]'.bold.red + ' Sorry, there was an issue locating your config.json. Please see README.md');
    process.exit();
  }
  done();
}

// Post images to AWS S3 so they are accessible to Litmus and manual test
function aws() {
  var publisher = !!CONFIG.aws ? $.awspublish.create(CONFIG.aws) : $.awspublish.create();
  var headers = {
    'Cache-Control': 'max-age=315360000, no-transform, public'
  };

  return gulp.src('./app/email/assets/img/*')
    // publisher will add Content-Length, Content-Type and headers specified above
    // If not specified it will set x-amz-acl to public-read by default
    .pipe(publisher.publish(headers))

    // create a cache file to speed up consecutive uploads
    //.pipe(publisher.cache())

    // print upload updates to console
    .pipe($.awspublish.reporter());
}

// Send email to Litmus for testing. If no AWS creds then do not replace img urls.
function litmus() {
  var awsURL = !!CONFIG && !!CONFIG.aws && !!CONFIG.aws.url ? CONFIG.aws.url : false;

  return gulp.src('app/email/**/*.html')
    .pipe($.if(!!awsURL, $.replace(/=('|")(\/?assets\/img)/g, "=$1" + awsURL)))
    .pipe($.litmus(CONFIG.litmus))
    .pipe(gulp.dest('app/email'));
}

// Send email to specified email for testing. If no AWS creds then do not replace img urls.
function mail() {
  var awsURL = !!CONFIG && !!CONFIG.aws && !!CONFIG.aws.url ? CONFIG.aws.url : false;

  if (EMAIL) {
    CONFIG.mail.to = [EMAIL];
  }

  return gulp.src('app/email/**/*.html')
    .pipe($.if(!!awsURL, $.replace(/=('|")(\/?assets\/img)/g, "=$1" + awsURL)))
    .pipe($.mail(CONFIG.mail))
    .pipe(gulp.dest('app/email'));
}

// Copy and compress into Zip
function zip() {
  var dist = 'app/email';
  var ext = '.html';

  function getHtmlFiles(dir) {
    return fs.readdirSync(dir)
      .filter(function (file) {
        var fileExt = path.join(dir, file);
        var isHtml = path.extname(fileExt) == ext;
        return fs.statSync(fileExt).isFile() && isHtml;
      });
  }

  var htmlFiles = getHtmlFiles(dist);

  var moveTasks = htmlFiles.map(function (file) {
    var sourcePath = path.join(dist, file);
    var fileName = path.basename(sourcePath, ext);

    var moveHTML = gulp.src(sourcePath)
      .pipe($.rename(function (path) {
        path.dirname = fileName;
        return path;
      }));

    var moveImages = gulp.src(sourcePath)
      .pipe($.htmlSrc({ selector: 'img' }))
      .pipe($.rename(function (path) {
        path.dirname = fileName + path.dirname.replace('app/email', '');
        return path;
      }));

    return merge(moveHTML, moveImages)
      .pipe($.zip(fileName + '.zip'))
      .pipe(gulp.dest('app/email'));
  });

  return merge(moveTasks);
}
