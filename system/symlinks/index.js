 
var ncp = require('ncp').ncp;
var fs = require('fs');

ncp.limit = 16;

 function checkExistDirectoryIfNotExist(path){
    const result = path ? path : "./";

    if (!fs.existsSync(result)){
        fs.mkdirSync(result);
    }
 }

 checkExistDirectoryIfNotExist("app");
 checkExistDirectoryIfNotExist("app/server");
 checkExistDirectoryIfNotExist("app/server/views/");
 checkExistDirectoryIfNotExist("app/server/mock/");
 checkExistDirectoryIfNotExist("app/server/public");
 checkExistDirectoryIfNotExist("app/server/configures");

 function copyDirectoryRecursive(source, destination){
    ncp( source,  destination, {filter:true},function (err) {
        if (err) {
          return console.error(err);
        }
        console.log('done!');
       });
 }

 copyDirectoryRecursive("src/server/views/","app/server/views")
 copyDirectoryRecursive("src/server/public/","app/server/public");
 copyDirectoryRecursive("src/interfaces/@types/","node_modules/@types");
 copyDirectoryRecursive("src/server/configures/config.json","app/server/configures/config.json");
 copyDirectoryRecursive("src/server/mock/mock.json","app/server/mock/mock.json");
 
 