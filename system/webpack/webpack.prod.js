let reactApps = require('./assets/webpack.prod.config.js');
const webpack = require("webpack");
const merge = require("webpack-merge");

module.exports = merge(reactApps);
