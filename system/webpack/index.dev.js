var webpack = require('webpack'),
	webpackDevMiddleware = require('webpack-dev-middleware'),
	webpackHotMiddleware = require('webpack-hot-middleware'),
	merge = require("webpack-merge"),
	config = require('./webpack.dev'),
	webpackconfig = merge(config,{watch:true}),
	webpackcompiler = webpack(webpackconfig);
	webpackcompiler.apply(new webpack.ProgressPlugin());
	

//enable webpack middleware for hot-reloads in development
function useWebpackMiddleware(app) {
	app.use(webpackDevMiddleware(webpackcompiler,{
		publicPath: webpackconfig.output.publicPath,
		serverSideRender:true,
		headers: { "X-Custom-Header": "yes" },
		stats: {
			colors: true,
			chunks: false, // this reduces the amount of stuff I see in my terminal; configure to your needs
			'errors-only': true
		}
	}));
	app.use(webpackHotMiddleware(webpackcompiler,  {
		log: console.log,
		dynamicPublicPath:false,
	}));

	return app;
}

module.exports = {
	useWebpackMiddleware: useWebpackMiddleware
};