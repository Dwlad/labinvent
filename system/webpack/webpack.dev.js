let reactApps = require('./assets/webpack.dev.config.js');
const webpack = require("webpack");
const merge = require("webpack-merge");

module.exports = merge(reactApps, {
	target: 'web'
});