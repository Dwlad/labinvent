const NODE_ENV = process.env.NODE_ENV || "development";
const webpack = require("webpack");
let path = require("path");
let AssetsPlugin = require("assets-webpack-plugin");
let rimraf = require("rimraf");
const precss = require("precss");
const autoprefixer = require("autoprefixer");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const globSync = require("glob").sync;
const readFileSync = require("fs").readFileSync;


// let hmr = NODE_ENV === "development" ?
// ["webpack-dev-server/client?http://192.168.100.20:8000",
// "webpack/hot/dev-server"] : [];
let hmr = [];

//const hmr = NODE_ENV !== 'production' ? ['react-hot!babel'] : [''];
module.exports = {
	cache: true,
	context: path.join(__dirname, "../../../src/client"),
	output: {
		path: path.join(__dirname, "../../../app/server/public/assets"),
		library: "[name]"
	},
	plugins: [
		new webpack.NoEmitOnErrorsPlugin(),
		new webpack.optimize.OccurrenceOrderPlugin(),
		new webpack.DefinePlugin({
			"process.env": {
				WEBPACK: JSON.stringify(true),
				LANG: JSON.stringify("en")
			}
		}),
		new webpack.optimize.CommonsChunkPlugin({
			name: "common",
			minChunks: 2
		}),
		/*new webpack.ProvidePlugin({
'React': 'react',
'ReactDOM': 'react-dom',
'ReactRouter': 'react-router',
"jQuery":"jquery"
}),*/
		new AssetsPlugin({
			filename: "assets.json",
			path: path.join(__dirname, "../../../app/server/public")
		}),
		// new webpack.optimize.DedupePlugin(), new
		// webpack.optimize.OccurenceOrderPlugin(),
		{
			apply: compiler => {
				"use strict";
				rimraf.sync(compiler.options.output.path);
			}
		}
	],
	resolve: {
		extensions: [".webpack.js", ".web.js", ".ts", ".tsx", ".js"],
		alias: {
			scss: path.join(__dirname, "../../../src/client/scss/"),
		
			indexApp: path.join(
				__dirname,
				"../../../src/client/typescript/react/indexApp"
			),
			Shared: path.join(
				__dirname,
				"../../../src/client/typescript/react/Shared"
			),
			client: path.join(__dirname, "../../../src/client/"),
			configures: path.join(__dirname, "../../../src/server/configures/"),
			shared: path.join(__dirname, "../../../src/server/shared/"),
			TS: path.join(__dirname, "../../../src/shared/TS")
		}
	},
	resolveLoader: {
		modules: ["node_modules"],
		moduleExtensions: ["*-loader", "*"],
		extensions: [".webpack.js", ".web.js", ".ts", ".tsx", ".js"]
	},

	externals: {
		jquery: "jQuery"
	}
};

// if (NODE_ENV === "production") {
// 	module.exports.plugins.push(
// 		new webpack.optimize.UglifyJsPlugin({
// 			sourceMap: false,
// 			compress: {
// 				warnings: false,
// 				drop_console: true,
// 				unsafe: true
// 			}
// 		})
// 	);
// }
