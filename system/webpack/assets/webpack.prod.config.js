const getRootPath = pathName => path.join(__dirname, "../../../" + pathName);

const merge = require("webpack-merge");
const common = require("./webpack.common.config");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const path = require("path");
const webpack = require("webpack");


module.exports = merge(common, {
	entry: {
		//React
		indexApp: ["whatwg-fetch", "./typescript/react/indexApp"],
		

		// //TS
		// globalJs: ["whatwg-fetch", "./typescript/scripts/global/global"],
		// socketJs: ["whatwg-fetch", "./typescript/scripts/socket/socket"],
		// indexPageJs: [
		// 	"whatwg-fetch",
		// 	"./typescript/scripts/pages/index/indexPage"
		// ],
		// registrationPageJs: [
		// 	"whatwg-fetch",
		// 	"./typescript/scripts/pages/registration/registrationPage"
		// ],

		// //SCSS
		// globalCss: ["whatwg-fetch", "./scss/global/_global.scss"],
		// indexPageCss: ["whatwg-fetch", "./scss/pages/index/_indexPage.scss"],
		// policyPageCss: ["whatwg-fetch", "./scss/pages/policy/_policy.scss"],
		// authPageCss: ["whatwg-fetch", "./scss/pages/auth/_auth.scss"],
		// rulesPageCss: ["whatwg-fetch", "./scss/pages/rules/_rules.scss"]
	},
	output: {
		publicPath: "/assets/",
		filename: "[name].[hash].min.js"
	},
	devtool: false,
	plugins: [
		new ExtractTextPlugin({
			filename: "[name].[hash].min.css",
			allChunks: true
		}),
		new webpack.optimize.UglifyJsPlugin(),
		new webpack.DefinePlugin({
			"process.env": {
				NODE_ENV: JSON.stringify("production"),
				SERVER: JSON.stringify("http://localhost:8080")
			}
		}),
		new webpack.DllReferencePlugin({
			context: getRootPath("src/client"),
			manifest: require(getRootPath("app/server/public/") +
				"vendor-manifest.json")
		})
	],
	module: {
		rules: [
			{
				test: /\.tsx?$/,
				include: getRootPath("src/client/typescript/react"),
				exclude: getRootPath("src/server"),
				use: [
					"babel-loader?presets[]=react,presets[]=env",
					"awesome-typescript-loader"
				]
			},
			{
				test: /\.tsx?$/,
				include: getRootPath("src/client/typescript/scripts"),
				exclude: getRootPath("src/server"),
				use: ["awesome-typescript-loader"]
				/*babelrc:false*/
			},
			{
				test: /\.tsx?$/,
				include: getRootPath("src/shared/TS"),
				exclude: getRootPath("src/server"),
				use: ["awesome-typescript-loader"]
				/*babelrc:false*/
			},
			{
				test: /\.ts$/,
				include: getRootPath("src/server"),
				exclude: getRootPath("src/client"),
				use: ["awesome-typescript-loader"]
				/*babelrc:false*/
			},
			{
				test: /\.scss$/,
				include: getRootPath("src/client/typescript/react"),
				exclude: getRootPath("src/server"),
				use: ExtractTextPlugin.extract({
					fallback: "style-loader",
					use:
					"css-loader?sourceMap&minimize=true!resolve-url-loader!sass-loader"
				})
			},
			{
				test: /\.css$/,
				use: ExtractTextPlugin.extract({
					fallback: "style-loader",
					use:
					"css-loader?sourceMap&minimize=true!resolve-url-loader"
				})
			},
			{
				test: /\.scss$/,
				include: getRootPath("src/client/scss"),
				exclude: getRootPath("src/server"),
				use: ExtractTextPlugin.extract({
					fallback: "style-loader",
					use:
					"css-loader?minimize=true!resolve-url-loader!sass-loader"
				})
			},
			{
				test: /\.(jpe?g|png|gif|svg)$/,
				exclude: getRootPath("src/server"),
				use: "file-loader?name=images/[name].[ext]"
			},
			{
				test: /\.json$/,
				loader: "json-loader"
			}
		]
	}
});
