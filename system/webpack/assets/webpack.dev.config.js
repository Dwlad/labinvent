const merge = require("webpack-merge");
const common = require("./webpack.common.config");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const path = require("path");
const webpack = require("webpack");


const getRootPath = pathName => path.join(__dirname, "../../../" + pathName);


const BitBarWebpackProgressPlugin = require("bitbar-webpack-progress-plugin");
var BundleAnalyzerPlugin = require("webpack-bundle-analyzer")
	.BundleAnalyzerPlugin;

function getEntry(path) {
	if (process.env.HMR) {
		return [
			"babel-polyfill",
			"whatwg-fetch",
			"react-hot-loader/patch",
			path,
			"webpack-hot-middleware/client",
			"webpack/hot/dev-server"
		];
	} else {
		return ["whatwg-fetch", path];
	}
}

module.exports = merge(common, {
	entry: {
		//React
		indexApp: getEntry("./typescript/react/indexApp"),

		// //TS
		// globalJs: getEntry("./typescript/scripts/global/global"),
		// socketJs: getEntry("./typescript/scripts/socket/socket"),
		// indexPageJs: getEntry("./typescript/scripts/pages/index/indexPage"),
		// registrationPageJs: getEntry(
		// 	"./typescript/scripts/pages/registration/registrationPage"
		// ),

		// //SCSS
		// globalCss: getEntry("./scss/global/_global.scss"),
		// indexPageCss: getEntry("./scss/pages/index/_indexPage.scss"),
		// policyPageCss: getEntry("./scss/pages/policy/_policy.scss"),
		// authPageCss: getEntry("./scss/pages/auth/_auth.scss"),
		// rulesPageCss: getEntry("./scss/pages/rules/_rules.scss")
	},
	output: {
		publicPath: "/assets/",
		filename: "[name].js"
	},
	watchOptions: {
		aggregateTimeout: 100,
		poll: true
	},
	devtool: process.env.HMR ? "eval" : "cheap-inline-module-source-map",
	plugins: [
		new ExtractTextPlugin({ filename: "[name].css", allChunks: true }),
		new webpack.DefinePlugin({
			"process.env": {
				NODE_ENV: JSON.stringify("development"),
				SERVER: JSON.stringify("http://localhost:8080")
			}
		}),
		new BitBarWebpackProgressPlugin() 
	],
	module: {
		rules: [
			{
				test: /\.tsx?$/,
				include: getRootPath("src/client/typescript/react"),
				exclude: getRootPath("app"),
				use: process.env.HMR
					? [
							"babel-loader?presets[]=react,presets[]=env",
							"react-hot-loader/webpack",
							//"webpack-module-hot-accept",
							"ts-loader"
						]
					: [
							"babel-loader?presets[]=react,presets[]=env",
							"ts-loader"
						]
				/*babelrc:false*/
			},
			{
				test: /\.ts$/,
				include: getRootPath("src/client/typescript/scripts"),
				exclude: getRootPath("app"),
				use: process.env.HMR
					? ["webpack-module-hot-accept", "ts-loader"]
					: ["ts-loader"]
				/*babelrc:false*/
			},
			{
				test: /\.ts$/,
				include: getRootPath("src/shared/TS"),
				exclude: getRootPath("app"),
				use: process.env.HMR
					? ["webpack-module-hot-accept", "ts-loader"]
					: ["ts-loader"]
				/*babelrc:false*/
			},
			{
				test: /\.ts$/,
				include: getRootPath("src/server"),
				exclude: getRootPath("app"),
				use: ["ts-loader"]
				/*babelrc:false*/
			},
			{
				test: /\.scss$/,
				include: getRootPath("src/client/typescript/react"),
				exclude: getRootPath("app"),
				use: [
					"style-loader",
					"css-loader?sourceMap!resolve-url-loader!sass-loader?outputStyle=expanded&sourceM" +
						"ap=true&sourceMapContents=true"
				]
				/*babelrc:false*/
			},
			{
				test: /\.css$/,
				use: [
					"style-loader",
					"css-loader?sourceMap!resolve-url-loader"
				]
				/*babelrc:false*/
			},
			{
				test: /\.scss$/,
				include: getRootPath("src/client/scss"),
				exclude: getRootPath("app"),
				use: [
					"style-loader",
					"css-loader?sourceMap!resolve-url-loader!sass-loader?outputStyle=expanded&sourceM" +
						"ap=true&sourceMapContents=true"
				]
				/*babelrc:false*/
			},
			{
				test: /\.(jpe?g|png|gif|svg)$/,
				exclude: getRootPath("src/server"),
				use: "file-loader?name=images/[name].[ext]"
			},
			{
				test: /\.json$/,
				loader: "json-loader"
			}
		]
	}
});

if (process.env.HMR) {
	module.exports.plugins.push(new webpack.HotModuleReplacementPlugin());
}
if (process.env.ANALYZE == "true") {
	module.exports.plugins.push(new BundleAnalyzerPlugin());
}
