const  webpack = require('webpack');
const rimraf = require("rimraf");
const path = require("path");
let AssetsPlugin = require("assets-webpack-plugin");

module.exports = {
	context: path.join(__dirname, "../../../src/client/"),
	entry: {
		vendor: ["./typescript/vendor"]
	},
	output: {
		path: path.join(__dirname, "../../../app/server/public/dll"),
		publicPath: '/dll/',
		library: "[name]",
		filename: "[name].min.js"
	},


	plugins: [
		new AssetsPlugin({
			filename: "assets_vendor.json",
			path: path.join(__dirname, "../../../app/server/public")
		}),
		{
			apply: (compiler) => {
				'use strict';
				rimraf.sync(compiler.options.output.path)
			}
		},
		new webpack.DllPlugin({
			path:path.join(__dirname, "../../../app/server/public")+'/[name]-manifest.json',
			name: '[name]'
		})
	],
	resolve: {
		extensions: [".webpack.js", ".web.js", ".ts", ".tsx", ".js"]
	},

	resolveLoader: {
		modules: ['node_modules'],
		moduleExtensions: ["*-loader", "*"],
		extensions: [".webpack.js", ".web.js", ".ts", ".tsx", ".js"]
	},
};